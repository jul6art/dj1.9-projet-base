from django.shortcuts import render, redirect
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from core.models import Analytic, MainSetting
from django import http

# Create your views here.

def registerx(request):
    if request.method == 'POST':
        form = UserCreationForm(request.POST)
        if form.is_valid():
            new_user = form.save()
            users = User.objects.filter(username=request.POST["username"])
            user=users[0]
            user.save()
            return redirect("/")
    else:
        form = UserCreationForm()
    return render(request, "register.html", {
        'form': form,
    })

def home(request):
    #gestion interne
    mainSetting = MainSetting.objects.get(id=1)
    if(mainSetting.maintenance == True) & (request.user.is_staff == False):
        return redirect("/maintenance")
    insideManaging(request)
    #fin de gestion interne

    return render(request, "core/index.html")

def fbCallback(request):
    return redirect("/")

def maintenance(request):
    mainSetting = MainSetting.objects.get(id=1)
    insideManaging(request)
    return render(request, 'core/maintenance.html', {'main': mainSetting})

def insideManaging(request):
    x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')
    if x_forwarded_for:
        ip = x_forwarded_for.split(',')[0]
    else:
        ip = request.META.get('REMOTE_ADDR')
    page = request.path
    analytic = Analytic(page=page, ip=ip)
    analytic.save()
