from django.db import models
from django.contrib.auth.models import User

class Analytic(models.Model):
    date = models.DateTimeField(auto_now_add=True, auto_now=False, verbose_name='Date de visite')
    page = models.CharField(max_length=255, null=True)
    ip = models.CharField(max_length=255, null=True)

    def __str__(self):
        return str(self.ip)

class MainSetting(models.Model):
    nom = models.CharField(null=True, default='Mon site Django 1.9', max_length=255)
    maintenance = models.BooleanField(default=0)

    def __str__(self):
        return str(self.nom)


