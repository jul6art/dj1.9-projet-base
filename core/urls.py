from django.conf.urls import patterns, url, include
from django.contrib.auth import views as auth_views
from core import views as views

from django.conf.urls.static import static
from django.conf import settings


urlpatterns = [
    url(r'^logout$', auth_views.logout, {'next_page': auth_views.login}, name='logoutx'),
    url(r'^login$', auth_views.login, {'template_name':'login.html'}, name='loginx'),
    url(r'^password_change/$', auth_views.password_change, {'template_name':'password_change.html'}, name='password_change'),
    url(r'^password_change_done/$', auth_views.password_change_done, {'template_name':'password_change_done.html'}, name='password_change_done'),
    url(r'^password_reset/$', auth_views.password_reset, {'template_name':'password_reset.html'}, name='password_reset'),
    url(r'^password_reset_done/$', auth_views.password_reset_done, {'template_name':'password_reset_done.html'}, name='password_reset_done'),
    url(r'^reset/(?P<uidb64>[0-9A-Za-z]+)/(?P<token>.+)/$', auth_views.password_reset_confirm, {'template_name':'password_reset_confirm.html'}, name='password_reset_confirm'),
    url(r'^reset/done/$', auth_views.password_reset_complete, {'template_name':'password_reset_complete.html'}, name='password_reset_complete'),
    url(r'^register/$', views.registerx, name='register'),
    url(r'^$', views.home, name='home'),
    url(r'^maintenance$', views.maintenance, name='maintenance'),
    url(r'^accounts/facebook/login/callback/', views.fbCallback, name='fbCallback'),
]