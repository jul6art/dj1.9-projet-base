from django.contrib import admin
from core.models import Analytic, MainSetting

class AnalyticAdmin(admin.ModelAdmin):
   list_display   = ('ip', 'page', 'date')
   list_filter    = ('ip', 'page', 'date')
   ordering       = ('ip', 'page', 'date', )
   search_fields  = ('ip', 'page', 'date')

class MainSettingAdmin(admin.ModelAdmin):
   list_display   = ('nom', 'maintenance')
   list_filter    = ('nom', 'maintenance')
   ordering       = ('nom', 'maintenance', )
   search_fields  = ('nom', 'maintenance')



admin.site.register(Analytic, AnalyticAdmin)
admin.site.register(MainSetting, MainSettingAdmin)
